let username = "Олександр";

let password = "secret";

let checkPassword = prompt(`Вітаємо, ${username}, введіть будь ласка свій пароль`);

console.log(checkPassword === password);

// or

// if (checkPassword === password) {
//     console.log("Ласкаво просимо.");
// } else {
//     console.log("Введено невірний пароль.");
// }

///////////////////////////////////////////////////////////////////////////////////

x = 5;
y = 3;

alert(`${x+y}  /  ${x-y}  /  ${x*y}  /  ${x/y}`);

// or

let addition = x+y;
let subtraction = x-y;
let multiplication = x*y;
let division = x/y;

alert(`${addition}  /  ${subtraction}  /  ${multiplication}  /  ${division}`);